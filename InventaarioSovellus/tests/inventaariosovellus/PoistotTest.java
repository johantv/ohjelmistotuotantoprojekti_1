package inventaariosovellus;

import static org.junit.Assert.*;

import org.junit.Test;

public class PoistotTest {

	// Queries
	@Test
	public void testPoistoID() {
		assertEquals("1", is.MySQLConnect.mySQLQuery("poistoID", "\"01/01/2001\"", "PoistoPaivamaara", "poistot"));
	}

	@Test
	public void testPoistoTuoteID() {
		assertEquals("4", is.MySQLConnect.mySQLQuery("tuoteID", "1", "poistoID", "poistot"));
	}

	@Test
	public void testPoistoMaara() {
		assertEquals("5", is.MySQLConnect.mySQLQuery("tuoteMaara", "1", "poistoID", "poistot"));
	}

	@Test
	public void testPoistoSelitys() {
		assertEquals("Testitarkoitus", is.MySQLConnect.mySQLQuery("poistoSelitys", "1", "poistoID", "poistot"));
	}

	@Test
	public void testPoistoKayttajaID() {
		assertEquals("1", is.MySQLConnect.mySQLQuery("kayttajaID", "1", "poistoID", "poistot"));
	}

	@Test
	public void testPoistoPaivamaara() {
		assertEquals("01/01/2001", is.MySQLConnect.mySQLQuery("poistoPaivamaara", "1", "poistoID", "poistot"));
	}
}
