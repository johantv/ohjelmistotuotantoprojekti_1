package inventaariosovellus;
import static org.junit.Assert.assertEquals;


import org.junit.Test;

/**
 *
 */

public class TuoteTest {

	//Tuotteet
	@Test
    public void testMySQLTuoteNimi() {
		System.out.println("\n testMySQLTuoteNimi:");
        assertEquals("Ruuvi", is.MySQLConnect.mySQLQuery("tuoteNimi", "1", "tuoteID", "tuotteet")); //kohde, hakuavain, hakurivi, taulu
        // lis�� error: "mySQL query ei toiminut"
    }

	@Test
	public void testMySQLTuoteIDByNimi() {
		System.out.println("\n testMySQLTuoteIDByNimi:");
		assertEquals("2", is.MySQLConnect.mySQLQuery("tuoteID", "\"FidgetSpinner\"", "tuoteNimi", "tuotteet"));
	}

	@Test
	public void testMySQLTuoteHinta() {
		System.out.println("\n testMySQLTuoteHinta:");
		assertEquals("10.00", is.MySQLConnect.mySQLQuery("tuoteHinta", "1", "tuoteID", "tuotteet"));

	}

	@Test
	public void testMySQLTuoteMaara() {
		System.out.println("\n testMySQLTuoteMaara:");
		assertEquals("10", is.MySQLConnect.mySQLQuery("TuoteMaara", "1", "tuoteID", "tuotteet"));
	}

	/*@Test
	public void testMySQLTuotePaivamaara() {
		System.out.println("\n testMySQLTuotePaivamaara:");
		assertEquals("11/12/2014", is.MySQLConnect.mySQLQuery("tuotePaivamaara", "1", "tuoteID", "tuotteet"));
	}*/

	// lis�ys, poisto ja update
	@Test
	public void testMySQLTuoteLisays() {
		System.out.println("\n testMySQLTuoteLisays:");
		is.MySQLConnect.mySQLInsertTuote(60, "Karviainen", 2.5, 30, 2);
		assertEquals("Karviainen", is.MySQLConnect.mySQLQuery("tuoteNimi", "60", "tuoteID", "tuotteet"));
		is.MySQLConnect.mySQLDelete(60, "tuoteID", "tuotteet");
	}

	@Test
	public void testMySQLTuotePoisto() {
		System.out.println("\n testMySQLTuotePoisto:");
		is.MySQLConnect.mySQLInsertTuote(61, "Karviainen", 2.5, 30, 2);
		assertEquals("Karviainen", is.MySQLConnect.mySQLQuery("tuoteNimi", "61", "tuoteID", "tuotteet"));
		is.MySQLConnect.mySQLDelete(61, "tuoteID", "tuotteet");
		assertEquals("", is.MySQLConnect.mySQLQuery("tuoteNimi", "61", "tuoteID", "tuotteet"));
	}

	@Test
	public void testMySQLPoistoLisays() {
		System.out.println("\n testMySQLPoistoLisays:");
		is.MySQLConnect.mySQLInsertPoisto(2, 3, "testMySQLPoistoLisays", "01/01/1990");
		assertEquals("testMySQLPoistoLisays", is.MySQLConnect.mySQLQuery("poistoSelitys", "\"01/01/1990\"", "poistoPaivamaara", "poistot"));
		int poistoID = Integer.parseInt(is.MySQLConnect.mySQLQuery("poistoID", "\"01/01/1990\"", "poistoPaivamaara", "poistot"));
		is.MySQLConnect.mySQLDelete(poistoID, "poistoID", "poistot");
		is.MySQLConnect.mySQLResetPoistotIncrement();
	}

	@Test
	public void testMySQLTuoteUpdate() {
		System.out.println("\n testMySQLTuoteUpdate:");
		is.MySQLConnect.mySQLInsertTuote(60, "Karviainen", 2.5, 30, 2);
		assertEquals("Karviainen", is.MySQLConnect.mySQLQuery("tuoteNimi", "60", "tuoteID", "tuotteet"));
		is.MySQLConnect.mySQLUpdateTuote(60, 67, "Karviainen2", 2.7, 31, 2);
		assertEquals("Karviainen2", is.MySQLConnect.mySQLQuery("tuoteNimi", "67", "tuoteID", "tuotteet"));
		is.MySQLConnect.mySQLDelete(67, "tuoteID", "tuotteet");
	}
}
