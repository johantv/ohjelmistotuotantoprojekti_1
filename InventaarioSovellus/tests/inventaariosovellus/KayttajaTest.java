package inventaariosovellus;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class KayttajaTest {
	// Kayttajat

		@Test
		public void testKayttajatunnusByID() {
			assertEquals("user1", is.MySQLConnect.mySQLQuery("kayttajatunnus", "1", "kayttajaID", "kayttajat"));
		}

		@Test
		public void testKayttajatunnusBySalasana() {
			assertEquals("user1", is.MySQLConnect.mySQLQuery("kayttajatunnus", "\"koira1\"", "salasana", "kayttajat"));
		}

		@Test
		public void testKayttajaSalasanaByID() {
			assertEquals("koira1", is.MySQLConnect.mySQLQuery("salasana", "1", "kayttajaID", "kayttajat"));
		}

		@Test
		public void testKayttajaSalasanaByKayttajatunnus() {
			assertEquals("koira1", is.MySQLConnect.mySQLQuery("salasana", "\"user1\"", "kayttajatunnus", "kayttajat"));
		}

		@Test
		public void testKayttajaIDByKayttajatunnus() {
			assertEquals("2", is.MySQLConnect.mySQLQuery("kayttajaid", "\"user2\"", "kayttajatunnus", "kayttajat"));
		}

		@Test
		public void testKayttajaIDBySalasana() {
			assertEquals("1", is.MySQLConnect.mySQLQuery("kayttajaid", "\"koira1\"", "salasana", "kayttajat"));
		}

}
