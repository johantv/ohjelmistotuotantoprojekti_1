package is.view;

import java.util.Locale;
import java.util.ResourceBundle;

import is.MainApp;
import is.MySQLConnect;
import is.model.Kayttaja;
import is.model.Tuote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

public class KirjautuminenController {
	private MainApp mainApp;
	@FXML
	private TextField kayttajaTunnusField;
	@FXML
	private PasswordField salasanaField;

	private Stage dialogStage;
	public boolean login = false;

	//lokalisaatio
	String messageProperties = "";
	String language, country = "";
	//public static String currentKayttajatunnus = "n/a";
	//public String IDString = MySQLConnect.mySQLQuery("kayttajaID", currentKayttajatunnus, "kayttajatunnus", "kayttajat");
	//public int currentKayttajaID = Integer.parseInt(IDString);

	public static int getCurrentKayttajaID() {
		return 1;
		//return MySQLConnect.mySQLQuery("kayttajaID", currentKayttajatunnus, "kayttajatunnus", "kayttajat");
	}


	@FXML
	private void initialize() {
	}

	/**
	 * Sets the stage of this dialog.
	 *
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}

	/**
	 * Sets the tuote to be edited in the dialog.
	 *
	 * @param tuote
	 */
	public void setTunnukset() {
		// this.tuote = tuote;
		kayttajaTunnusField.setText("");
		salasanaField.setText("");
	}

	/**
	 * Returns true if the user clicked "Kirjaudu", false otherwise.
	 *
	 * @return
	 */
	public boolean isLogin() {
		return login;
	}

	/**
	 * Called when the user clicks "Kirjaudu" or presses Enter
	 */
	@FXML
	private void handleLogin() {
		String kayttajatunnus = kayttajaTunnusField.getText();
		String salasana = salasanaField.getText();
		// vertaa annettua salasanaa annetun kayttajatunnuksen salasanaan
		// tietokannassa
		String vertausSalasana = MySQLConnect.mySQLQuery("salasana", "\"" + kayttajatunnus + "\"", "kayttajatunnus",
				"kayttajat");
		if (isInputValid()) {
			if (/* vertausKayttajatunnus.compareTo(kayttajatunnus) == 0 && */ vertausSalasana
					.compareTo(salasana) == 0) {
				System.out.println("Kirjautuminen onnistui");
				login = true;
				dialogStage.close();
			} else {
				// n�ytt�� error pop-up:in
				Alert alert = new Alert(AlertType.ERROR);
				String errorMessage = "Invalid username or password";
				alert.initOwner(dialogStage);
				alert.setTitle("Invalid username or password");
				alert.setHeaderText("Please enter valid password");
				alert.setContentText(errorMessage);
				alert.showAndWait();

				System.out.println("K�ytt�jatunnukset eivat t�sm��");

			}
		}
		// testik�ytt�iset printlinet
		System.out.println("K�ytt�j�tunnus: " + kayttajaTunnusField.getText());
		// System.out.println("Verrattava k�ytt�j�tunnus: " +
		System.out.println("Salasana: " + salasanaField.getText());
		System.out.println("Verrattava salasana: " + vertausSalasana);
		// System.out.println("Verrattava ID: " + vertausID);
	}


	// tarkistaa onko annetut merkit hyv�ksytt�vi�
	private boolean isInputValid() {
		String errorMessage = "";
		if (kayttajaTunnusField.getText() == null || kayttajaTunnusField.getText().length() == 0) {
			errorMessage += "No valid username!\n";
		}
		if (salasanaField.getText() == null || salasanaField.getText().length() == 0) {
			errorMessage += "No valid password!\n";
		}
		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Invalid Fields");
			alert.setHeaderText("Please correct invalid fields");
			alert.setContentText(errorMessage);
			alert.showAndWait();
			return false;
		}
	}

	// vaihtaa lokalisaation annettujen parametrien mukaan
	public void languageChange(String language, String country) {
		this.country = country;
		this.language = language;
		messageProperties = "Messages_" + getLanguage() + "_" + getCountry();


		MainApp.messages = ResourceBundle.getBundle(messageProperties, new Locale(language, country));

		//kielen vaihto
		mainApp.logout();
		dialogStage.close();
		
		
		System.out.println("Testitarkoitukseen (messageProperties): " + messageProperties);
	}
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public String getLanguageFile() {
		return messageProperties;
	}

	public String getLanguage() {
		return language;
	}
	public String getCountry() {
		return country;
	}

	@FXML
	private void handleFinnish() {
		languageChange("fi", "FI");
	}
	@FXML
	private void handleEnglish() {
		languageChange("en", "EN");
	}
	@FXML
	private void handleSpanish() {
		languageChange("es", "ES");
	}
}
