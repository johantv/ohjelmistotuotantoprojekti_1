package is.view;

import javafx.beans.property.SimpleIntegerProperty;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Font;

import java.awt.Color;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import is.MainApp;
import is.MySQLConnect;
import is.model.Tuote;

public class VarastoOverviewController {
	@FXML
	private TableView<Tuote> tuoteTable;
	@FXML
	private TableColumn<Tuote, Number> tuoteIDColumn;
	@FXML
	private TableColumn<Tuote, String> tuoteNimiColumn;
	@FXML
	private TableColumn<Tuote, Number> tuoteMaaraColumn;
	/*@FXML
	private TableColumn<Tuote, Number> tuoteHintaColumn;
	@FXML
	private TableColumn<Tuote, Number> tuoteArvolaskentaColumn;*/

	@FXML
	private Label tuoteIDLabel;
	@FXML
	private Label tuoteNimiLabel;
	@FXML
	private Label tuoteHintaLabel;
	@FXML
	private Label tuoteMaaraLabel;
	@FXML
	private Label hyllyIDLabel;

	// Reference to the main application.
	private MainApp mainApp;
	
	private Tuote tuote;
	private static String loppuvat = "";
	private static int kokArvo = 0;

	/**
	 * The constructor. The constructor is called before the initialize() method.
	 */
	public VarastoOverviewController() {
	}

	/**
	 * Initializes the controller class. This method is automatically called after
	 * the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		// Initialize the tuote table with the two columns.
		tuoteIDColumn.setCellValueFactory(cellData -> cellData.getValue().tuoteIDProperty());
		tuoteNimiColumn.setCellValueFactory(cellData -> cellData.getValue().tuoteNimiProperty());
		tuoteMaaraColumn.setCellValueFactory(cellData -> cellData.getValue().tuoteMaaraProperty());
		

		// Clear tuote details;
		showVarastoDetails(null);
		

		// Listen for selection changes and show the tuote details when changed.
	    tuoteTable.getSelectionModel().selectedItemProperty().addListener(
	            (observable, oldValue, newValue) -> showVarastoDetails(newValue));
	    
	    
	    showVarastoDetails(null);
		
	}

	private void showVarastoDetails(Tuote tuote) {
		if (tuote != null) {
			
			// Fill the labels with info from the tuote object.
			tuoteNimiLabel.setText(tuote.getTuoteNimi());
			tuoteIDLabel.setText(Integer.toString(tuote.getTuoteID()));
			tuoteHintaLabel.setText(Double.toString(tuote.getTuoteHinta()));
			tuoteMaaraLabel.setText(Integer.toString(tuote.getTuoteMaara()));
			hyllyIDLabel.setText(Integer.toString(tuote.getHyllyID()));
			
		} else {
			// Tuote is null, remove all the text.
			tuoteIDLabel.setText("");
			tuoteNimiLabel.setText("");
			tuoteHintaLabel.setText("");
			tuoteMaaraLabel.setText("");
			hyllyIDLabel.setText("");
		}
		
		
	}

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteTuote() {
	    int selectedIndex = tuoteTable.getSelectionModel().getSelectedIndex();
	    Tuote poistettavaTuote = tuoteTable.getSelectionModel().getSelectedItem();
	    int poistettavaTuoteID = poistettavaTuote.getTuoteID();
	    MySQLConnect.mySQLDelete(poistettavaTuoteID, "tuoteID", "tuotteet");
	    tuoteTable.getItems().remove(selectedIndex);
	}

	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new tuote.
	 */
	@FXML
	private void handleNewTuote() {
	    Tuote tempTuote = new Tuote();
	    boolean okClicked = mainApp.showTuoteEditDialog(tempTuote);
	    if (okClicked) {
	        mainApp.getTuoteData().add(tempTuote);
	    }
	}

	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit
	 * details for the selected tuote.
	 */
	@FXML
	private void handleEditTuote() {
	    Tuote selectedTuote = tuoteTable.getSelectionModel().getSelectedItem();
	    if (selectedTuote != null) {
	        boolean okClicked = mainApp.showTuoteEditDialog(selectedTuote);
	        if (okClicked) {
	            showVarastoDetails(selectedTuote);
	        }

	    } else {
	        // Nothing selected.
	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("Tuotetta ei ole valittu");
	        alert.setContentText("Valitse tuote");

	        alert.showAndWait();
	    }
	}
	
	@FXML
	private void handleKokonaisArvo() {
		//mainApp.showKokonaisArvo();
		laskeKokonaisArvo();
	}

	@FXML
	private void handleAbout() {
		mainApp.showAbout();
	}

	@FXML
	private void handleKirjauduUlos() {
		mainApp.logout();
	}

	/**
	 * Called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;

		// Add observable list data to the table
		tuoteTable.setItems(mainApp.getTuoteData());
		annaVaroitusLoppuvista();
	}
	
	private void laskeKokonaisArvo() {
		mySQLHaeTuotteet();
		
		//luo ilmoitus
		Alert tuoteAlert = new Alert(AlertType.INFORMATION);
		tuoteAlert.initOwner(mainApp.getPrimaryStage());
	    tuoteAlert.setTitle("Varaston kokonaisarvo");
	    tuoteAlert.setHeaderText("Varaston kokonaisarvo on tällä hetkellä: ");
	    	
	    tuoteAlert.setContentText(kokArvo + " euroa.");

	    tuoteAlert.showAndWait();
	    loppuvat = "";
	    kokArvo = 0;
		
	}
	private void annaVaroitusLoppuvista() {
				
		mySQLHaeTuotteet();
		
		//luo varoitus
		Alert tuoteAlert = new Alert(AlertType.INFORMATION);
		tuoteAlert.initOwner(mainApp.getPrimaryStage());
	    tuoteAlert.setTitle("Tilaa tuotteita lisää!");
	    tuoteAlert.setHeaderText("Seuraavat tuotteet vähissä:");
	    	
	    tuoteAlert.setContentText(loppuvat);

	    tuoteAlert.showAndWait();
	    loppuvat = "";
	    kokArvo = 0;
	}
	
	
	public static ObservableList<Tuote> mySQLHaeTuotteet() {

		ObservableList<Tuote> loppuvatList = FXCollections.observableArrayList();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// kayttajatunnus/salasana
			Connection conn = DriverManager.getConnection("jdbc:mysql://haxers.ddns.net:3306/varastodb", "monty", "metrofilia1");
			String query = "SELECT * FROM tuotteet";
			// create the java statement
			Statement st = conn.createStatement();
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);
			ObservableList<Tuote> tuoteData = FXCollections.observableArrayList();
			// iterate through the java resultset
			while (rs.next()) {
				int id = rs.getInt("TuoteID");
				String nimi = rs.getString("TuoteNimi");
				int hinta = rs.getInt("TuoteHinta");
				int maara = rs.getInt("TuoteMaara");
				int hyllyID = rs.getInt("HyllyID");
				tuoteData.add(new Tuote(id, nimi, hinta, maara, hyllyID));
				System.out.format("%s, %s, %s, %s, %s  \n", id, nimi, hinta, maara, hyllyID);
				if (maara < 6) {
					loppuvat = loppuvat + "\n" + nimi;
				}
				kokArvo += hinta;
			}
			st.close();
			return tuoteData;
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return null;
	}
	
	
}
