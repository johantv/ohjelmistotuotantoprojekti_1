/**
 *
 */
package is.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Date;

import is.MySQLConnect;
import is.model.Tuote;

/**
 * @author ras
 *
 */
public class TuoteEditDialogController {
	    @FXML
	    private TextField tuoteIDField;
	    @FXML
	    private TextField tuoteNimiField;
	    @FXML
	    private TextField tuoteHintaField;
	    @FXML
	    private TextField tuoteMaaraField;
	    @FXML
	    private TextField hyllyIDField;


	    private Stage dialogStage;
	    private Tuote tuote;
	    private boolean okClicked = false;

	    /**
	     * Initializes the controller class. This method is automatically called
	     * after the fxml file has been loaded.
	     */
	    @FXML
	    private void initialize() {
	    }

	    /**
	     * Sets the stage of this dialog.
	     *
	     * @param dialogStage
	     */
	    public void setDialogStage(Stage dialogStage) {
	        this.dialogStage = dialogStage;
	    }

	    /**
	     * Sets the tuote to be edited in the dialog.
	     *
	     * @param tuote
	     */
	    public void setTuoteField(Tuote tuote) {
	        this.tuote = tuote;
	        tuoteIDField.setText(Integer.toString(tuote.getTuoteID()));
	        tuoteNimiField.setText(tuote.getTuoteNimi());
	        tuoteHintaField.setText(Double.toString(tuote.getTuoteHinta()));
	        tuoteMaaraField.setText(Integer.toString(tuote.getTuoteMaara()));
	        hyllyIDField.setText(Integer.toString(tuote.getHyllyID()));
	    }

	    /**
	     * Returns true if the user clicked OK, false otherwise.
	     *
	     * @return
	     */
	    public boolean isOkClicked() {
	        return okClicked;
	    }

	    /**
	     * Called when the user clicks ok.
	     */
	    @FXML
	    private void handleOk() {
	        if (isInputValid()) {
	        	int vanhaID = tuote.getTuoteID();
	            tuote.setTuoteID(Integer.parseInt(tuoteIDField.getText()));
	            tuote.setTuoteNimi(tuoteNimiField.getText());
	            tuote.setTuoteHinta(Double.parseDouble(tuoteHintaField.getText()));
	            tuote.setTuoteMaara(Integer.parseInt(tuoteMaaraField.getText()));
	            tuote.setHyllyID(Integer.parseInt(hyllyIDField.getText()));
	            if (vanhaID > 0) {
	            	MySQLConnect.mySQLUpdateTuote(vanhaID, tuote.getTuoteID(), tuote.getTuoteNimi(), tuote.getTuoteHinta(), tuote.getTuoteMaara(), tuote.getHyllyID());
	            }
	            else {
	            	MySQLConnect.mySQLInsertTuote(tuote.getTuoteID(), tuote.getTuoteNimi(), tuote.getTuoteHinta(), tuote.getTuoteMaara(), tuote.getHyllyID());
	            }
	            okClicked = true;
	            dialogStage.close();
	        }
	    }

	    /**
	     * Called when the user clicks cancel.
	     */
	    @FXML
	    private void handleCancel() {
	        dialogStage.close();
	    }

	    /**
	     * Validates the user input in the text fields.
	     *
	     * @return true if the input is valid
	     */
	    private boolean isInputValid() {
	        String errorMessage = "";

	        if (tuoteIDField.getText() == null || tuoteIDField.getText().length() == 0) {
	            errorMessage += "ID ei kelpaa!\n";
	        }
	        if (tuoteNimiField.getText() == null || tuoteNimiField.getText().length() == 0) {
	            errorMessage += "Tuotenimi ei kelpaa!\n";
	        }
	        if (tuoteHintaField.getText() == null || tuoteHintaField.getText().length() == 0) {
	            errorMessage += "tuoteHinta ei kelpaa!\n";
	        }

	        if (tuoteMaaraField.getText() == null || tuoteMaaraField.getText().length() == 0) {
	            errorMessage += "Tuotem��r� ei kelpaa!\n";
	        } else {
	            // miksi?
	            try {
	                Integer.parseInt(tuoteMaaraField.getText());
	            } catch (NumberFormatException e) {
	                errorMessage += "Tuotem��r� ei kelpaa (pit�� olla kokonaisluku)!\n";
	            }
	        }

	        if (hyllyIDField.getText() == null || hyllyIDField.getText().length() == 0) {
	            errorMessage += "HyllyID ei kelpaa!\n";
	        }

	        if (errorMessage.length() == 0) {
	            return true;
	        } else {
	            // Show the error message.
	            Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(dialogStage);
	            alert.setTitle("Invalid Fields");
	            alert.setHeaderText("Please correct invalid fields");
	            alert.setContentText(errorMessage);
	            alert.showAndWait();
	            return false;
	        }
	    }
	}
