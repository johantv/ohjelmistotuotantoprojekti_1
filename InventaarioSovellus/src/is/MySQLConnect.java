package is;

import java.sql.Connection;

import java.sql.DriverManager;

import java.sql.ResultSet;

import java.sql.Statement;

import is.model.Kayttaja;
import is.model.Tuote;
import is.view.KirjautuminenController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MySQLConnect {
	// jdbc driver - java/mysql connector
	static String myDriver = "com.mysql.cj.jdbc.Driver";
	// mysql serverin URL ja tietokanta
	static String myUrl = "jdbc:mysql://haxers.ddns.net:3306/varastodb";

	// Hakee mitä tahansa mistä tahansa taulusta
	public static String mySQLQuery(String kohdeColumn, String hakusana, String hakuColumn, String taulu) {
		String vastaus = "";
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			String query = "SELECT " + kohdeColumn + " FROM " + taulu + " WHERE " + hakuColumn + " = " + hakusana;
			// create the java statement
			System.out.println("Query: " + query);
			Statement st = conn.createStatement();
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				vastaus = rs.getString(kohdeColumn);
				System.out.println("Vastaus: " + vastaus);
			}
			if (vastaus == "") {
				System.out.println("Vastaus: " + "ei löytynyt");
			}
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return vastaus;
	}

	// Syöttää uuden tuotteen
	public static void mySQLInsertTuote(int tuoteID, String tuoteNimi, double tuoteHinta, int tuoteMaara, int hyllyID) {
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			String insert = "INSERT INTO tuotteet VALUES (" + tuoteID + ", \"" + tuoteNimi + "\", " + tuoteHinta + ", "
					+ tuoteMaara + ", " + hyllyID + ")";
			// create the java statement
			System.out.println("Insert: " + insert);
			Statement st = conn.createStatement();
			// execute the query
			st.executeUpdate(insert);
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// Muokkaa tuotetta
	public static void mySQLUpdateTuote(int vanhaTuoteID, int tuoteID, String tuoteNimi, double tuoteHinta,
			int tuoteMaara, int hyllyID) {
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			String update = "UPDATE tuotteet SET tuoteID = " + tuoteID + ", tuoteNimi = \"" + tuoteNimi
					+ "\", tuoteHinta = " + tuoteHinta + ", tuoteMaara = " + tuoteMaara + ", hyllyID = " + hyllyID + " WHERE tuoteID = " + vanhaTuoteID;
			// create the java statement
			System.out.println("Update: " + update);
			Statement st = conn.createStatement();
			// execute the update
			st.executeUpdate(update);
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// Poistaa tuotteen
	public static void mySQLDelete(int ID, String column, String taulu) {
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			Statement st = conn.createStatement();
			String delete = "DELETE FROM " + taulu + " WHERE " + column + " = " + ID;
			// create the java statement
			System.out.println("Delete: " + delete);
			st.executeUpdate(delete);
			String selitys = "automatic deletion";
			String paivamaara = "23/11/2018";
			//mySQLInsertPoisto(ID, tuoteMaara, selitys, paivamaara);
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

	}

	// Lisää tuotteen poiston poistot -tauluun
	public static void mySQLInsertPoisto(int tuoteID, int tuoteMaara, String selitys, String paivamaara) {
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			Statement st = conn.createStatement();
			int kayttajaID = KirjautuminenController.getCurrentKayttajaID();
			System.out.println("mySQLInsertPoisto (KayttajaID): " + kayttajaID);
			String insert = "INSERT INTO poistot VALUES (NULL" + ", " + tuoteID + ", "
			+ tuoteMaara + ", \"" + selitys + "\", " + kayttajaID + ", \"" + paivamaara + "\")";
			// create the java statement
			System.out.println("Insert (poisto): " + insert);
			// execute the query
			st.executeUpdate(insert);
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// reset poistot taulun autoincrement
	public static void mySQLResetPoistotIncrement() {
		try {
			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			Statement st = conn.createStatement();
			String alter = "ALTER TABLE poistot AUTO_INCREMENT = 1";
			st.executeUpdate(alter);
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// mySQL testi
	public static void mySQLConnect() {
		try {
			Class.forName(myDriver);
			// kayttajatunnus/salasana
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			String query = "SELECT * FROM tuotteet";
			// create the java statement
			Statement st = conn.createStatement();
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);
			// iterate through the java resultset
			while (rs.next()) {
				int id = rs.getInt("TuoteID");
				String firstName = rs.getString("TuoteNimi");
				int hinta = rs.getInt("TuoteHinta");
				// print the results
				System.out.format("%s, %s, %s \n", id, firstName, hinta);
			}
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	// hakee kaikki tuotteet
	public static ObservableList<Tuote> mySQLHaeTuotteet() {
		try {
			Class.forName(myDriver);
			// kayttajatunnus/salasana
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			String query = "SELECT * FROM tuotteet";
			// create the java statement
			Statement st = conn.createStatement();
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);
			ObservableList<Tuote> tuoteData = FXCollections.observableArrayList();
			// iterate through the java resultset
			while (rs.next()) {
				int id = rs.getInt("TuoteID");
				String nimi = rs.getString("TuoteNimi");
				int hinta = rs.getInt("TuoteHinta");
				int maara = rs.getInt("TuoteMaara");
				int hyllyID = rs.getInt("HyllyID");
				tuoteData.add(new Tuote(id, nimi, hinta, maara, hyllyID));
				System.out.format("%s, %s, %s, %s, %s  \n", id, nimi, hinta, maara, hyllyID);
			}
			st.close();
			return tuoteData;
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return null;
	}

	// mahdollisesti turha
	public static ObservableList<Kayttaja> mySQLVertaaKirjautuminen() {
		try {
			Class.forName(myDriver);
			// kayttajatunnus/salasana
			Connection conn = DriverManager.getConnection(myUrl, "monty", "metrofilia1");
			// testihaku
			String query = "SELECT * FROM kayttajat";
			// create the java statement
			Statement st = conn.createStatement();
			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);
			ObservableList<Kayttaja> kayttajatunnukset = FXCollections.observableArrayList();
			// iterate through the java resultset
			while (rs.next()) {
				int id = rs.getInt("KayttajaID");
				String nimi = rs.getString("KokoNimi");
				String kayttajatunnus = rs.getString("KayttajaNimi");
				String salasana = rs.getString("Salasana");
				int ryhmaID = rs.getInt("RyhmäID");
				kayttajatunnukset.add(new Kayttaja(id, nimi, kayttajatunnus, salasana, ryhmaID));
				// print the results
				System.out.format("%s, %s, %s, %s, %s  \n", id, nimi,
						kayttajatunnus, salasana,
						ryhmaID/* lastName, dateCreated, isAdmin, numPoints */);
			}
			st.close();
			return kayttajatunnukset;
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return null;

	}

}
