package is;

import java.io.IOException;
import java.util.Locale;

import is.model.Tuote;
import is.view.KirjautuminenController;
import is.view.TuoteEditDialogController;
import is.view.VarastoOverviewController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.util.ResourceBundle;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	public static ResourceBundle messages;

	// WIP: siirra Tuote -luokkaan
	private ObservableList<Tuote> tuoteData = FXCollections.observableArrayList();

	public static void main(String[] args) {
		launch(args);
	}



	public MainApp() {
		// WIP: siirra Tuote -luokkaan
		tuoteData = MySQLConnect.mySQLHaeTuotteet();
	}

	// returns the main stage
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	// WIP: siirra Tuote -luokkaan
	// palauttaa kaikki tuotteet tietokannassa
	public ObservableList<Tuote> getTuoteData() {
		return tuoteData;
	}

	// ... THE REST OF THE CLASS ...

	@Override
	public void start(Stage primaryStage) {
		Locale currentLocale;
		//localization
		String language = "fi";
		String country = "FI";
		String messagesFI = "Messages_fi_FI";

		currentLocale = new Locale(language, country);
		messages = ResourceBundle.getBundle(messagesFI, new Locale(language, country));
		this.primaryStage = primaryStage;
		//this.primaryStage.setTitle("SuperInventaarioSovellus"); VANHA, ilman lokalisointia
		this.primaryStage.setTitle(messages.getString("SIS"));

		kirjaudu();
	}

	// initializes the root layout
	public void initRootLayout() {
		try {
			// load root layout from fxml file
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/is/view/RootLayout.fxml"), messages);
			//loader.setLocation(getClass().getResource("/is/view/RootLayout.fxml"), messages);
			rootLayout = (BorderPane) loader.load();

			// show the scene containing the root layout
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shows the person overview inside the root layout.
	 */

	public void showVarastoOverview() {
		try {
			// Load person overview.
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/is/view/RootLayout.fxml"), messages);
			loader.setLocation(getClass().getResource("/is/view/VarastoOverview.fxml"));
			AnchorPane varastoOverview = (AnchorPane) loader.load();

			// Set varasto overview into the center of root layout.
			rootLayout.setCenter(varastoOverview);

			// Give the controller access to the main app.
			VarastoOverviewController controller = loader.getController();
			controller.setMainApp(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// avaa showTuoteEditDialog. Jos kayttaja painaa ok, tallentaa tiedot olioon ja palauttaa true
	public boolean showTuoteEditDialog(Tuote tuote) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/is/view/RootLayout.fxml"), messages);
			loader.setLocation(MainApp.class.getResource("/is/view/TuoteEditDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle(messages.getString("Muokkaa"));
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the product into the controller.
			TuoteEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setTuoteField(tuote);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void kirjaudu() {
		if (showKirjautuminen() == true) {
			initRootLayout();
			showVarastoOverview();
		}
	}

	public boolean showKirjautuminen() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/is/view/RootLayout.fxml"), messages);
			loader.setLocation(MainApp.class.getResource("view/Kirjautuminen.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle(messages.getString("Sisaankirjautuminen")); //lis�tty lokalisaatio
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Controller
			KirjautuminenController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setTunnukset();
			controller.setMainApp(this);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isLogin();
		} catch (

		IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void logout() {
		primaryStage.close();
		kirjaudu();
	}

	public void showAbout() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/About.fxml"));
			BorderPane page = (BorderPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle(messages.getString("About"));
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showKokonaisArvo() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/KokonaisArvo.fxml"));
			BorderPane page = (BorderPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle(messages.getString("Kokonaisarvo"));
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 *
	 * // shows the varasto overview inside the root layout
	 *
	 * public void showVarastoOverview() { try { // load varasto overview
	 * FXMLLoader loader = new FXMLLoader();
	 * loader.setLocation(getClass().getResource("/is/view/VarastoOverview.fxml"
	 * )); AnchorPane varastoOverview = (AnchorPane) loader.load();
	 *
	 * // set varasto overview into the center of root layout
	 * rootLayout.setCenter(varastoOverview); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 */
}
