/**
 *
 */
package is.model;

import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author ras
 *
 */
@Entity
@Table(name = "kayttajat")
public class KayttajaHibernate {

	private static SessionFactory factory;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	// ...
	@OneToMany
	@JoinColumn(name = "tilaus_id", nullable = false)
	private Tilaus tilaus;
	@OneToMany
	@JoinColumn(name = "tuotehaku_id", nullable = false)
	private Tuotehaku tuotehaku;

	private int kayttajaID;
	private String nimi;
	private String kayttajatunnus;
	private String salasana;
	private int ryhmaID;

	public KayttajaHibernate() {
	}


	// konstruktori joka antaa automaattisesti seuraavan mahdollisen ID:n
	public KayttajaHibernate(String nimi, String kayttajatunnus, String salasana, int ryhmaID) {
		this.nimi = nimi;
		this.kayttajatunnus = kayttajatunnus;
		this.salasana = salasana;
		this.ryhmaID = ryhmaID;
	}

	private static ObservableList<Kayttaja> kayttajaData = FXCollections.observableArrayList();

	public static ObservableList<Kayttaja> getKayttajaData() {
		return kayttajaData;
	}

	 public static void addKayttaja(String nimi, String kayttajatunnus, String salasana, int ryhmaID){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      try {
	         tx = session.beginTransaction();
	         KayttajaHibernate kayttaja = new KayttajaHibernate(nimi, kayttajatunnus, salasana, ryhmaID);
	        session.save(kayttaja);
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace();
	      } finally {
	         session.close();
	      }
	   }

	   /* Method to  READ all the employees */
	   public static void listKayttajat( ){
	      Session session = factory.openSession();
	      Transaction tx = null;

	      try {
	         tx = session.beginTransaction();
	         List employees = session.createQuery("FROM KayttajaHibernate").list();
	         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
	            KayttajaHibernate kayttaja = (KayttajaHibernate) iterator.next();
	            System.out.print("Nimi: " + kayttaja.getNimi());
	            System.out.print("Kayttajatunnus: " + kayttaja.getKayttajatunnus());
	            System.out.print("Salasana: " + kayttaja.getSalasana());
	            System.out.println("  Salary: " + kayttaja.getRyhmaID());
	         }
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace();
	      } finally {
	         session.close();
	      }
	   }

	   /* Method to UPDATE ryhmaID for an kayttaja */
	   public static void updateEmployee(Integer kayttajaID, int ryhmaID ){
	      Session session = factory.openSession();
	      Transaction tx = null;

	      try {
	         tx = session.beginTransaction();
	         KayttajaHibernate kayttaja = (KayttajaHibernate)session.get(KayttajaHibernate.class, kayttajaID);
	         kayttaja.setRyhmaID( ryhmaID );
			 session.update(kayttaja);
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace();
	      } finally {
	         session.close();
	      }
	   }

	   /* Method to DELETE an kayttaja from the records */
	   public static void deleteEmployee(Integer kayttajaID){
	      Session session = factory.openSession();
	      Transaction tx = null;

	      try {
	         tx = session.beginTransaction();
	         KayttajaHibernate kayttaja = (KayttajaHibernate)session.get(KayttajaHibernate.class, kayttajaID);
	         session.delete(kayttaja);
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace();
	      } finally {
	         session.close();
	      }
	   }

	public int getKayttajaID() {
		return kayttajaID;
	}

	public void setKayttajaID(int kayttajaID) {
		this.kayttajaID = kayttajaID;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public String getKayttajatunnus() {
		return kayttajatunnus;
	}

	public void setKayttajatunnus(String kayttajatunnus) {
		this.kayttajatunnus = kayttajatunnus;
	}

	public String getSalasana() {
		return salasana;
	}

	public void setSalasana(String salasana) {
		this.salasana = salasana;
	}

	public int getRyhmaID() {
		return ryhmaID;
	}

	public void setRyhmaID(int ryhmaID) {
		this.ryhmaID = ryhmaID;
	}

}