/**
 *
 */
package is.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author ras
 *
 */
@Entity
@Table(name="tuotehaut")
public class Tuotehaku {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    //...
    @OneToOne
    @JoinColumn(name="tuote_id", nullable=false)
    private Tuote tuote;
    public Tuotehaku() {}
}
