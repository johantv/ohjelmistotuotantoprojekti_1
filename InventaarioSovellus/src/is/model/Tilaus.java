/**
 *
 */
package is.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;




/**
 * @author ras
 *
 */
@Entity
@Table(name="tilaukset")
public class Tilaus {
  @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    //...
    @OneToMany
    @JoinColumn(name="tuote_id", nullable=false)
    private Tuote tuote;
    public Tilaus() {}
}