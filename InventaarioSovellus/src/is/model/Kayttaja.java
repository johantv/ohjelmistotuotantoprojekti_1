/**
 *
 */
package is.model;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author ras
 *
 */
@Entity
@Table(name = "kayttaja")
public class Kayttaja {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	// ...
	@OneToMany
	@JoinColumn(name = "tilaus_id", nullable = false)
	private Tilaus tilaus;
	@OneToMany
	@JoinColumn(name = "tuotehaku_id", nullable = false)
	private Tuotehaku tuotehaku;

	private IntegerProperty kayttajaID;
	private StringProperty nimi;
	private StringProperty kayttajatunnus;
	private StringProperty salasana;
	private IntegerProperty ryhmaID;

	public Kayttaja() {
	}

	public Kayttaja(int kayttajaID, String nimi, String kayttajatunnus, String salasana, int ryhmaID) {
		this.kayttajaID = new SimpleIntegerProperty(kayttajaID);
		this.nimi = new SimpleStringProperty(nimi);
		this.kayttajatunnus = new SimpleStringProperty(kayttajatunnus);
		this.salasana = new SimpleStringProperty(salasana);
		this.ryhmaID = new SimpleIntegerProperty(ryhmaID);
	}

	// konstruktori joka antaa automaattisesti seuraavan mahdollisen ID:n
	// ei toimi, koska java fx toteutus vaatii integer/stringproperty, eik� string/int
	/*public Kayttaja(String nimi, String kayttajatunnus, String salasana, int ryhmaID) {
		this.nimi = nimi;
		this.kayttajatunnus = kayttajatunnus;
		this.salasana = salasana;
		this.ryhmaID = ryhmaID;
	}*/

	private static ObservableList<Kayttaja> kayttajaData = FXCollections.observableArrayList();

	public static ObservableList<Kayttaja> getKayttajaData() {
		return kayttajaData;
	}

	public IntegerProperty getKayttajaID() {
		return kayttajaID;
	}

	public void setKayttajaID(IntegerProperty kayttajaID) {
		this.kayttajaID = kayttajaID;
	}

	public StringProperty getNimi() {
		return nimi;
	}

	public void setNimi(StringProperty nimi) {
		this.nimi = nimi;
	}

	public StringProperty getKayttajatunnus() {
		return kayttajatunnus;
	}

	public void setKayttajatunnus(StringProperty kayttajatunnus) {
		this.kayttajatunnus = kayttajatunnus;
	}

	public StringProperty getSalasana() {
		return salasana;
	}

	public void setSalasana(StringProperty salasana) {
		this.salasana = salasana;
	}

	public IntegerProperty getRyhmaID() {
		return ryhmaID;
	}

	public void setRyhmaID(IntegerProperty ryhmaID) {
		this.ryhmaID = ryhmaID;
	}
}