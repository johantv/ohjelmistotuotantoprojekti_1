package is.model;

import javax.persistence.*;


import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;
import org.hibernate.mapping.Set;

import javafx.beans.property.DoubleProperty;

//import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
//import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
//import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Tuote {

	private IntegerProperty tuoteID;
	private StringProperty tuoteNimi;
	private SimpleDoubleProperty tuoteHinta;
	private IntegerProperty tuoteMaara;
	private IntegerProperty hyllyID;

	/**
	 * Default constructor.
	 */
	public Tuote() {
		this(0, "", 0, 0, 0);
	}

	public Tuote(Integer tuoteID, String tuoteNimi, double tuoteHinta, Integer tuoteMaara, Integer hyllyID) {
		this.tuoteID = new SimpleIntegerProperty(tuoteID);
		this.tuoteNimi = new SimpleStringProperty(tuoteNimi);
		this.tuoteHinta = new SimpleDoubleProperty(tuoteHinta);
		this.tuoteMaara = new SimpleIntegerProperty(tuoteMaara);
		this.hyllyID = new SimpleIntegerProperty(hyllyID);
	}

	public int getTuoteID() {
		return tuoteID.get();
	}

	public void setTuoteID(int tuoteID) {
		this.tuoteID.set(tuoteID);
	}

	public IntegerProperty tuoteIDProperty() {
		return tuoteID;
	}

	public String getTuoteNimi() {
		return tuoteNimi.get();
	}

	public void setTuoteNimi(String tuoteNimi) {
		this.tuoteNimi.set(tuoteNimi);
	}

	public StringProperty tuoteNimiProperty() {
		return tuoteNimi;
	}

	public double getTuoteHinta() {
		return tuoteHinta.get();
	}

	public void setTuoteHinta(double tuoteHinta) {
		this.tuoteHinta.set(tuoteHinta);
	}

	public DoubleProperty tuoteHintaProperty() {
		return tuoteHinta;
	}

	public int getTuoteMaara() {
		return tuoteMaara.get();
	}

	public void setTuoteMaara(int tuoteMaara) {
		this.tuoteMaara.set(tuoteMaara);
	}

	public IntegerProperty tuoteMaaraProperty() {
		return tuoteMaara;
	}

	public int getHyllyID() {
		return hyllyID.get();
	}

	public void setHyllyID(int hyllyID) {
		this.hyllyID.set(hyllyID);
	}

	public IntegerProperty hyllyIDProperty() {
		return hyllyID;
	}

	/*
	@Entity
    @Table(prnKey="topics", appliesTo = "")
    public class Tuote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nimi")
    private String nimi;
    public int getId() {
    return id;
    }
    @OneToMany(mappedBy= "topic")
    private Set messages;
	}*/
}
