package is;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import demo.Employee;
import is.model.Kayttaja;
import is.model.KayttajaHibernate;

public class Main2 {
	private static SessionFactory factory;

	public static void main(String[] args) {

		//MySQLConnect.mySQLConnect();
		//MySQLConnect.test();

		// vanhan mySQL Driver manuaalinen testi
		oldMySQLTest();



		// käyttää hibernatea "Turo Puro" käyttäjän tekemiseen
		/*
		System.out.println("Hibernate testi");
		 try {
	         factory = new Configuration().configure().buildSessionFactory();
	      } catch (Throwable ex) {
	         System.err.println("Failed to create sessionFactory object." + ex);
	         throw new ExceptionInInitializerError(ex);
	      }
		 addKayttaja("Turo Puro", "turo94", "1994", 6);
	*/

	}

	 /* Method to CREATE an employee in the database with Hibernate*/
	   public static void addKayttaja(String nimi, String kayttajatunnus, String salasana, int ryhmaID){
	      Session session = factory.openSession();
	      Transaction tx = null;
	      //Integer employeeID = null;

	      try {
	         tx = session.beginTransaction();
	         KayttajaHibernate kayttaja = new KayttajaHibernate(nimi, kayttajatunnus, salasana, ryhmaID);
	         session.save(kayttaja);
	         tx.commit();
	      } catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace();
	      } finally {
	         session.close();
	      }
	   }

	   // käyttää mysql driveriä suoraan muutamien mysql käskyjen tekemiseen testimielessä
	   public static void oldMySQLTest() {
		    /*System.out.println(MySQLConnect.mySQLQuery("tuoteNimi", "1", "tuoteID",
					"tuotteet")); //hakukohde, hakuavain, hakuavaimen rivi, taulu

			System.out.println("ID test");
			String idTest = MySQLConnect.mySQLQuery("tuoteID", "\"FidgetSpinner\"", "tuoteNimi", "tuotteet");

			System.out.println(idTest);*/

			MySQLConnect.mySQLInsertTuote(16, "PoistoTesti", 30.0, 16, 2);
			System.out.println("Done inserting!");
			//MySQLConnect.mySQLUpdateTuote(6, 6, "Karviainen2", 30.5, 15, 2, "01/01/2030");
			//System.out.println(MySQLConnect.mySQLQuery("tuoteNimi", "6", "tuoteID", "tuotteet"));
			MySQLConnect.mySQLDelete(16, "tuoteiD", "tuotteet");
			System.out.println("Done deleting!");

			//System.out.println(MySQLConnect.mySQLQuery("kayttajatunnus", "\"koira1\"", "salasana", "tuotteet"));
	   }
}
