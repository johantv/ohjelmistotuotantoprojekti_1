package demo;
/**
 * 
 */

/**
 * @author ras
 *
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table
public class Student {
     
    @Id
    @GeneratedValue
    private Integer id;
     
    private String firstName;
    private Integer age;
     
    public Student() {};
     
    public Student(Integer id, String firstName, Integer age) {
        this.id = id;
        this.firstName = firstName;
        this.age = age;
    }

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param i
	 */
	public void setAge(int i) {
		// TODO Auto-generated method stub
		
	}
     
        //Here you need to generate getters and setters
 
}